<?php
namespace Fhsinchy\Inspire\Controllers;

use Illuminate\Http\Request;
use Fhsinchy\Inspire\Inspire;

class InspirationController
{
    public function HelloView(Inspire $inspire) {
        $quote = $inspire->justDoIt();

        return $quote;
    }
}