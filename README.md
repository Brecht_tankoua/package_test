## Package Test ##

### Installation ###

3 - The next required step is to add the service provider to config/app.php :
```
    'Fhsinchy\Inspire\Providers\InspirationProvider::class',
```

### Publish ###

The last required step is to publish views and assets in your application with :
```
    php artisan vendor:publish
```

Congratulations, you have successfully installed Scafold !